<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<#assign title="与客服进行对话"/>
<#include "resource.ftl"/>
</head>

<body>
<div id="chat" style="width: 100%;">
    <div id="header" style="background-color: #13758c;color: #ffffff;padding:8px;font-size: 14px;line-height: 32px;overflow: hidden;text-overflow: ellipsis;white-space:nowrap"><template v-if="conversation == null">分配客服中.</template><template v-else>客服[{{conversation.supportUserEntity.nickName}}]为您服务<span class="btn" style="border: 1px solid #ffffff;margin-left: 8px;" @click="findSupport(1)">切换</span></template></div>


    <div class="message-content" onscroll="onScroll()" style="-webkit-overflow-scrolling: touch;height: calc(100% - 135px);overflow-y: auto" >
        <div v-if="messages.length>0 && canLoadMore == 1" @click="loadMore" style="text-align: center;font-size: 12px;color: #9e9e9e;padding: 6px;background-color: #f9f9f9;">点击加载更多</div>
        <div v-if="canLoadMore == 2" style="text-align: center;font-size: 12px;color: #9e9e9e;padding: 6px;background-color: #f9f9f9;"><i class="fa fa-refresh fa-spin fa-fw"></i>正在加载</div>

        <div class="list">
            <template v-for="(message,index) of messages">
                <template v-if="message.type == 999">
                    <div class="message system">
                    <div v-html="message.content" class=" text" ></div>
                    </div>
                </template>

                <template v-else>
                    <div :class="message.reciver == lastConversation.supportUserEntity.messageId?'mine':'other'" class="message time" v-bind:time="new Date(message.createTime).format('yyyy-MM-dd hh:mm:ss')">
                        <img v-if="message.reciver != lastConversation.supportUserEntity.messageId" :src="lastConversation.userEntity.avatar" onerror="this.src='/imgs/default.png'" class="avatar"  />
                        <div class="text" v-if="message.type == 1">{{message.content}}</div>
                        <div class="text" v-if="message.type == 2" ><img :browser="message.content" :src="message.content+'?imageView2/0/w/200'" @click="showPrototypeImage($event)" style="max-width: 200px;"></div>
                        <img v-if="message.reciver == lastConversation.supportUserEntity.messageId" :src="lastConversation.userEntity.avatar" onerror="this.src='/imgs/default.png'" class="avatar"  />
                    </div>
                </template>
            </template>
        </div>
    </div>

    <div id="toolbox" style="position: absolute;bottom: 0;width: 100%;">
        <div style="border-top:1px solid #e1e1e1;padding: 4px 8px;">
            <i id="image-sender" class="iconfont icon-tupian" style="color: #656565"></i>
            <i class="iconfont icon-lianjie1" style="color: #656565;margin-left: 8px;"></i>
        </div>
        <div style="padding: 4px 8px;" :style="{cursor:conversation == null?'not-allowed':''}">
            <div v-bind:contenteditable="conversation == null?'false':'true'" id="content" onkeydown="onKeyDown()"
                 style="font-size:12px;color: #000;height: 32px;line-height:16px;overflow-y: auto;outline: none;"></div>
            <div style="margin-top: 8px;text-align: right">
                <button class="btn btn-info" onclick="onSend()">发送</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/js/mobile.js"></script>
</body>
</html>
