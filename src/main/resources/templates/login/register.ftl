<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <#assign title="Athena客服管理系统 | 登陆"/>
<#include "/resource.ftl"/>
</head>

<body>
<div style="width: 100%;">
    <div class="login-form">
        <form class="form-signin">
            <h2 class="form-signin-heading">客服端注册</h2>
            <h4 class="form-signin-heading">（本页面仅供演示测试使用，大侠们请不要恶意注册手下留情）</h4>
            <br/>
            <div class="form-inputs">
                <input type="text" name="userName"  v-model="userName" class="form-control" placeholder="请输入账号" required="" autofocus="">
                <input type="password" name="userPwd" v-model="userPwd" class="form-control" placeholder="请输入密码" required="">
            </div>
            <br/>
            <button class="btn btn-lg btn-primary btn-block" type="submit">注册</button>
        </form>
    </div>
</div>
<script>

    var vm = new Vue({
        el: '.login-form',
        data: {
            userName: '',
            userPwd: ''
        }
    });
</script>
</body>
</html>
