package com.opdar.athena.support.base;

import com.opdar.platform.core.base.Context;
import com.opdar.platform.core.base.Interceptor;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Created by shiju on 2017/1/24.
 */
public class AppidInterceptor implements Interceptor,ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public boolean before() {
        if(Context.getRequest().getParameterMap().containsKey("appId")){
            return true;
        }
        return false;
    }

    @Override
    public boolean after() {
        return true;
    }

    @Override
    public void finish() {

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
