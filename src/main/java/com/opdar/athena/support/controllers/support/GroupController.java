package com.opdar.athena.support.controllers.support;

import com.opdar.athena.support.base.CommonInterceptor;
import com.opdar.athena.support.base.Result;
import com.opdar.athena.support.entities.GroupEntity;
import com.opdar.athena.support.entities.SupportUserEntity;
import com.opdar.athena.support.mapper.GroupMapper;
import com.opdar.athena.support.service.SupportUserService;
import com.opdar.platform.annotations.Interceptor;
import com.opdar.platform.annotations.Request;
import com.opdar.platform.core.base.Context;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

/**
 * Created by shiju on 2017/7/13.
 */
@Controller
@Interceptor(CommonInterceptor.class)
public class GroupController {

    @Autowired
    GroupMapper groupMapper;
    @Autowired
    SupportUserService supportUserService;

    @Request(value = "/group/list", format = Request.Format.JSON)
    public Result listGroup() {
        GroupEntity group = new GroupEntity();
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        String appId = user.getAppId();
        group.setAppId(appId);
        return Result.valueOf(groupMapper.selectList(group));
    }

    @Request(value = "/group/create", format = Request.Format.JSON)
    public Result createGroup(String groupName) {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        String appId = user.getAppId();
        GroupEntity groupEntity = new GroupEntity();
        groupEntity.setId(UUID.randomUUID().toString());
        groupEntity.setCreateTime(new Timestamp(System.currentTimeMillis()));
        groupEntity.setUpdateTime(new Timestamp(System.currentTimeMillis()));
        groupEntity.setName(groupName);
        groupEntity.setCreatorId(user.getId());
        groupEntity.setAppId(appId);
        groupMapper.insert(groupEntity);
        return Result.valueOf(groupEntity);
    }

    @Request(value = "/group/delete", format = Request.Format.JSON)
    public Result deleteGroup(String groupId) {
        GroupEntity groupEntity = new GroupEntity();
        groupEntity.setId(groupId);
        groupMapper.delete(groupEntity);
        return Result.valueOf(null);
    }


    @Request(value = "/group/user/list", format = Request.Format.JSON)
    public Result listSupport(String groupId) {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        String appId = user.getAppId();
        List<SupportUserEntity> supports = supportUserService.findAllSupport(groupId, appId);
        return Result.valueOf(supports);
    }

    @Request(value = "/group/user/register", format = Request.Format.JSON)
    public Object register(String groupId, String userName, String userPwd, String avatar,String nickName,Integer weight) {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        String appId = user.getAppId();
        SupportUserEntity support = supportUserService.registToGroup(groupId, userName, userPwd,nickName,weight, avatar, "SUPPORT", appId);
        if (support != null) {
            supportUserService.init(appId);
            return Result.valueOf(support);
        }
        return Result.valueOf(1, "");
    }

    @Request(value = "/group/user/update", format = Request.Format.JSON)
    public Object update(String supportId,String userName, String userPwd, String avatar,String nickName,Integer weight) {
        SupportUserEntity user = (SupportUserEntity) Context.getRequest().getAttribute("user");
        String appId = user.getAppId();
        supportUserService.updateToGroup(supportId, userName, userPwd,nickName,weight, avatar);
        supportUserService.init(appId);
        return Result.valueOf(null);
    }
}
