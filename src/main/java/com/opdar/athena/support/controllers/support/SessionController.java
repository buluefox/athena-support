package com.opdar.athena.support.controllers.support;

import com.alibaba.fastjson.JSONObject;
import com.opdar.athena.support.base.CommonInterceptor;
import com.opdar.athena.support.base.Result;
import com.opdar.athena.support.entities.SupportUserEntity;
import com.opdar.athena.support.entities.UserEntity;
import com.opdar.athena.support.utils.MessageUtils;
import com.opdar.athena.support.utils.qiniu.Auth;
import com.opdar.platform.annotations.Interceptor;
import com.opdar.platform.annotations.Request;
import com.opdar.platform.core.base.Context;
import com.opdar.platform.core.session.ISessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shiju on 2017/7/14.
 */
@Controller
public class SessionController {
    @Autowired
    ISessionManager<SupportUserEntity> sessionManager;
    @Autowired
    MessageUtils messageUtils;

    @Value("${athena.sock.protocol}")
    private String sockProtocol;
    @Value("${athena.sock.address}")
    private String sockAddress;
    @Value("${athena.web.address}")
    private String webAddress;
    @Value("${athena.upload.domain}")
    private String imageDomain;
    @Value("${qiniu.ak}")
    private String AK;
    @Value("${qiniu.sk}")
    private String SK;
    private Auth auth;
    @PostConstruct
    public void init(){
        auth = Auth.create(AK,SK);
    }


    @Interceptor(CommonInterceptor.class)
    @Request(value = "/session/info", format = Request.Format.JSON)
    public Object info(){
        Object user = Context.getRequest().getAttribute("user");
        return Result.valueOf(user);
    }

    @Request(value = "/session/check", format = Request.Format.JSON)
    public Result checkSession(String token){
        //login
        Integer object = messageUtils.check(token);
        if(object != null && object == 0){
            return Result.valueOf(null);
        }
        return Result.valueOf(1,"");
    }

    @Interceptor(CommonInterceptor.class)
    @Request(value = "/session/config", format = Request.Format.JSON)
    public Result config(){
        //login
        Map<String,String> hashMap = new HashMap<String, String>();
        hashMap.put("sockProtocol",sockProtocol);
        hashMap.put("sockAddress",sockAddress);
        hashMap.put("webAddress",webAddress);
        hashMap.put("imageDomain",imageDomain);

        return Result.valueOf(hashMap);
    }

    @Interceptor(CommonInterceptor.class)
    @Request(value = "/upload/token", format = Request.Format.JSON)
    public Result uploadToken(String bucket, String key){
        return Result.valueOf(auth.uploadToken(bucket));
    }

    /**
     * 获取一个交互用的Token
     * @return
     */
    @Interceptor(CommonInterceptor.class)
    @Request(value = "/session/get", format = Request.Format.JSON)
    public Result getSession(){
        //login
        Object user = Context.getRequest().getAttribute("user");
        String userId = null;
        String messagePwd = null;
        if(user instanceof SupportUserEntity){
            userId = ((SupportUserEntity) user).getId();
             messagePwd = ((SupportUserEntity) user).getMessagePwd();
        }else if (user instanceof UserEntity){
            userId = ((UserEntity) user).getId();
            messagePwd = ((UserEntity) user).getMessagePwd();
        }
        JSONObject object = messageUtils.login(userId,messagePwd,0);
        if(object != null){
            if(object.containsKey("token")){
                return Result.valueOf(object.getString("token"));
            }
        }
        return Result.valueOf(100,"您的账号好像出了点问题，请联系管理员。");
    }
}
