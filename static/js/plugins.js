$.fn.extend({
    browser:function () {
        var _move,_x,_y;
        var imageUrl = $(this).attr('browser');
        var prototypeWidth = 0;
        var prototypeHeight= 0;
        var bodyWidth = $(document.body).width();
        var bodyHeight= document.body.scrollHeight;
        var imageBrowser = $('<div class="image-browser" style="position: fixed;width: 100%;height: 100%;top: 0;">' +
            '<img ondragstart="return false;" style="position: absolute;cursor: all-scroll;z-index: 1000;display: none;" src="'+imageUrl+'" />' +
            '<div style="width: 100%;height: 100%;background-color: black;position: fixed; opacity: 0.8; filter: blur(30px);z-index: 999;background: url(\''+imageUrl+'\');background-size: cover" class="background"></div>' +
            '</div>');
        imageBrowser.find('.background').click(function () {
            $(this).parent().fadeOut(function () {
                $(this).remove();
            });
        });
        imageBrowser.find('img').load(function () {
            prototypeWidth = $(this).width();
            prototypeHeight = $(this).height();
            $(this).css('left',((bodyWidth-prototypeWidth)/2)+'px');
            $(this).css('top',((bodyHeight-prototypeHeight)/2)+'px');
            $(this).fadeIn();
        });

        imageBrowser.mousewheel(function(event) {
            if(!_move){
                var image = $(this).find('img');
                var left = parseInt(image.css('left'));
                var top = parseInt(image.css('top'));
                var w = image.width();
                var h = image.height();
                var scaleWdith = w*(1+0.05*event.deltaY);
                var scaleHeight= h*(1+0.05*event.deltaY);
                if(scaleWdith <= prototypeWidth * 2.0 && scaleWdith >= prototypeWidth*0.5){
                    image.css('width',scaleWdith);
                    var offsetX = (scaleWdith - w)/2;
                    var offsetY = (scaleHeight - h)/2;
                    image.css('left',(left-offsetX)+'px');
                    image.css('top',(top-offsetY)+'px');
                }
            }
        });
        imageBrowser.find('img').mousedown(function(e){
            _move=true;
            _x=e.pageX-parseInt($(this).css("left"));
            _y=e.pageY-parseInt($(this).css("top"));
            $(this).fadeTo(20, 0.7);//点击后开始拖动并透明显示
        });
        imageBrowser.find('img').mousemove(function(e){
            if(_move){
                var x=e.pageX-_x;//移动时根据鼠标位置计算控件左上角的绝对位置
                var y=e.pageY-_y;
                $(this).css({top:y,left:x});//控件新位置
            }
        });
        imageBrowser.find('img').mouseup(function(){
            _move=false;
            $(this).fadeTo("fast", 1);//松开鼠标后停止移动并恢复成不透明
        });
        $(document.body).append(imageBrowser);
    },
    QiniuUpload: function (options, tokenUrl) {
        var init = $(this).data('init');
        if(init){
            return;
        }else{
            $(this).data('init',true);
        }
        var defaults = {
            key: '',
            callback: function (result) {
            },
            error: function () {
            }
        };
        options = $.extend(defaults, options);
        var createFileFunc = function (width, height) {
            var file = $('<input type="file" style="display: none;" name="file" class="q_upload" />');
            file.css({
                position: 'absolute',
                left: '0',
                width: width + 'px',
                top: '0',
                height: height + 'px',
                filter: 'alpha(opacity=0)',
                opacity: '0',
                cursor: 'pointer'
            });
            file.change(function () {
                var uploadFile = $(this);
                var height = uploadFile.height();
                var width = uploadFile.width();
                var uploadFileParent = $(this).parent();
                $.post(tokenUrl, {}, function (result) {
                    var token = result;
                    var fileName = createUUID();

                    var qiniuUploadUrl;
                    if (window.location.protocol === 'https:') {
                        qiniuUploadUrl = 'https://up.qbox.me';
                    } else {
                        qiniuUploadUrl = 'http://upload.qiniu.com';
                    }
                    var form = $('<form method="post" action="'+qiniuUploadUrl+'" enctype="multipart/form-data">' +
                        '<input name="key" type="hidden" value="' + fileName + '">' +
                        '<input name="token" type="hidden" value="' + token + '">' +
                        '<input name="accept" type="hidden" value="text/plain" />' +
                        '</form>');
//                        var upFile = $('<input type="file" name="file" value="'+uploadFile.val()+'">');
                    form.append(uploadFile);
                    uploadFileParent.append(createFileFunc(width, height));
                    form.ajaxSubmit(function (result) {
                        uploadFileParent.trigger('callback', result);
                    });
                }).error(function () {
                    uploadFileParent.trigger('error');
                });
            });
            return file;
        };
        $(this).each(function (i, element) {
            var btn = $(element);
            var height = btn.height();
            var width = btn.width();
            btn.wrap('<div style="position: relative;display: inline;"></div>');
            var upBtn = btn.parent();
            var file = createFileFunc(width, height);

            upBtn.append(file);
            btn.unbind('click').bind('click',function () {
                $(this).next().trigger('click');
            });
            upBtn.unbind('callback').bind('callback', options.callback);
            upBtn.unbind('error').bind('error', options.error);
        });
    }
});